export const state = () => ({
  token: null
})

export const getters = {
  hasToken: s => !!s.token
}

export const mutations = {
  setToken: (s, token) => s.token = token,
  clearToken: s => s.token = null
}

export const actions = {
  nuxtServerInit() {
    console.log('nuxtServerInit')
  },

  login ({ commit }) {
    commit('setToken', 'anytoken')
  },

  logout ({ commit }) {
    commit('clearToken')
  }
}
