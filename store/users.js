export const state = () => ({
  users: []
})

export const getters = {
  users: s => s.users
}

export const mutations = {
  setUsers: (s, users) => s.users = users
}

export const actions = {
  async getAllUsers({ commit }) {
    const users = (await this.$axios.get('https://jsonplaceholder.typicode.com/users')).data
    commit('setUsers', users)
  }
}
